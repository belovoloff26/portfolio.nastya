interface IMeta {
  pagination: {
    limit: number
    start: number
    total: number
  }
}

export interface IWorkResponse {
  id: number
  attributes: {
    title: string
    type: string
    image: {
      data: {
        attributes: {
          url: string
        }
      }
    }
  }
}

export interface IWorksDataResponse extends IMeta {
  data: IWorkResponse[]
}

export interface IWork {
  url: string
  title?: string
  type?: string
}