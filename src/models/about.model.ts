export interface IAboutResponse {
  id: number
  attributes: {
    description: string
    height: string
    weight: string
    bust: string
    waist: string
    hips: string
    dressSize: string
    shoeSize: string
    colorEyes: string
    faceType: string
    hairColor: string
    hairSize: string
  }
}

export interface IAboutDataResponse {
  data: IAboutResponse
}