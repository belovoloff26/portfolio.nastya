export interface IContactsResponse {
  id: number
  attributes: {
    email: string
    phone: string
    inst: string
  }
}

export interface IContactsDataResponse {
  data: IContactsResponse
}